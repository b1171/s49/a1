import { useContext, useEffect, useState } from "react";
import { Container, Form, Button } from "react-bootstrap";
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";
import UserContext from "../UserContext";

export default function Register() {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [cPassword, setCPassword] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);

	const { user } = useContext(UserContext);

	useEffect(() => {
		if (
			email !== "" &&
			password !== "" &&
			cPassword !== "" &&
			password === cPassword
		) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password, cPassword]);

	function Register(e) {
		e.preventDefault();

		alert("Registration successful!");
	}

	if (user.email == null) {
		return (
			<Container className="my-5">
				<Form className="border p-3" onSubmit={(e) => Register(e)}>
					<Form.Group className="mb-3" controlId="email">
						<Form.Label>Email address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={(e) => {
								setEmail(e.target.value);
							}}
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="password">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Password"
							value={password}
							onChange={(e) => {
								setPassword(e.target.value);
							}}
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="cpassword">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Confirm Password"
							value={cPassword}
							onChange={(e) => {
								setCPassword(e.target.value);
							}}
						/>
					</Form.Group>

					<Button
						variant="primary"
						type="submit"
						disabled={isDisabled}
					>
						Submit
					</Button>
				</Form>
			</Container>
		);
	} else {
		return <Redirect to="/courses" />;
	}
}
