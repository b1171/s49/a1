import Banner from "../components/Banner";

export default function Error() {
    return(
        <Banner content="error" />
    );
}