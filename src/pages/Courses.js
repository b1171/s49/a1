import CourseCard from "../components/CourseCard";
import courseData from "../data/courseData";

export default function Courses() {
	return courseData.map((course) => {
		return (
			<CourseCard
				title={course.name}
				description={course.description}
				price={course.price}
			/>
		);
	});
}
